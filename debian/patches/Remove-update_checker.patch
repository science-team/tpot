From: Christian Kastner <ckk@kvr.at>
Date: Fri, 7 Feb 2020 12:41:41 +0100
Subject: Remove update_checker

Remove the update checker feature for now. It doesn't make sense for a
system-wide installation.

Forwarded: not-needed
---
 docs_sources/api.md        | 16 ----------------
 docs_sources/installing.md |  2 --
 setup.py                   |  1 -
 tests/driver_tests.py      |  1 -
 tests/tpot_tests.py        |  1 -
 tpot/base.py               | 10 ----------
 tpot/driver.py             | 13 +------------
 7 files changed, 1 insertion(+), 43 deletions(-)

diff --git a/docs_sources/api.md b/docs_sources/api.md
index 13e7254..8dabb6b 100644
--- a/docs_sources/api.md
+++ b/docs_sources/api.md
@@ -16,7 +16,6 @@
                           <strong>periodic_checkpoint_folder</strong>=None,
                           <strong>early_stop</strong>=None,
                           <strong>verbosity</strong>=0,
-                          <strong>disable_update_check</strong>=False,
                           <strong>log_file</strong>=None
                           </em>)</pre>
 <div align="right"><a href="https://github.com/EpistasisLab/tpot/blob/master/tpot/base.py">source</a></div>
@@ -220,13 +219,6 @@ Possible inputs are:
 </ul>
 </blockquote>
 
-<strong>disable_update_check</strong>: boolean, optional (default=False)
-<blockquote>
-Flag indicating whether the TPOT version checker should be disabled.
-<br /><br />
-The update checker will tell you when a new version of TPOT has been released.
-</blockquote>
-
 <strong>log_file</strong>: file-like class (io.TextIOWrapper or io.StringIO) or string, optional (default: None)
 <br /><br />
 <blockquote>
@@ -525,7 +517,6 @@ The whole pipeline text as a string should be returned if output_file_name is no
                          <strong>periodic_checkpoint_folder</strong>=None,
                          <strong>early_stop</strong>=None,
                          <strong>verbosity</strong>=0,
-                         <strong>disable_update_check</strong>=False</em>)</pre>
 <div align="right"><a href="https://github.com/EpistasisLab/tpot/blob/master/tpot/base.py">source</a></div>
 
 Automated machine learning for supervised regression tasks.
@@ -728,13 +719,6 @@ Possible inputs are:
 <li>3, TPOT will print everything and provide a progress bar.</li>
 </ul>
 </blockquote>
-
-<strong>disable_update_check</strong>: boolean, optional (default=False)
-<blockquote>
-Flag indicating whether the TPOT version checker should be disabled.
-<br /><br />
-The update checker will tell you when a new version of TPOT has been released.
-</blockquote>
 </td>
 </tr>
 
diff --git a/docs_sources/installing.md b/docs_sources/installing.md
index 5962730..beddb95 100644
--- a/docs_sources/installing.md
+++ b/docs_sources/installing.md
@@ -10,8 +10,6 @@ TPOT is built on top of several existing Python libraries, including:
 
 * [DEAP](https://github.com/DEAP/deap)
 
-* [update_checker](https://github.com/bboe/update_checker)
-
 * [tqdm](https://github.com/tqdm/tqdm)
 
 * [stopit](https://github.com/glenfant/stopit)
diff --git a/setup.py b/setup.py
index 96997ef..78e3dc7 100644
--- a/setup.py
+++ b/setup.py
@@ -39,7 +39,6 @@ This project is hosted at https://github.com/EpistasisLab/tpot
                     'scipy>=1.3.1',
                     'scikit-learn>=0.22.0',
                     'deap>=1.2',
-                    'update_checker>=0.16',
                     'tqdm>=4.36.1',
                     'stopit>=1.1.1',
                     'pandas>=0.24.2',
diff --git a/tests/driver_tests.py b/tests/driver_tests.py
index 86a1b68..b359b30 100644
--- a/tests/driver_tests.py
+++ b/tests/driver_tests.py
@@ -237,7 +237,6 @@ class ParserTest(TestCase):
         self.assertEqual(args.CONFIG_FILE, None)
         self.assertEqual(args.CROSSOVER_RATE, 0.1)
         self.assertEqual(args.EARLY_STOP, None)
-        self.assertEqual(args.DISABLE_UPDATE_CHECK, False)
         self.assertEqual(args.GENERATIONS, 100)
         self.assertEqual(args.INPUT_FILE, 'tests/tests.csv')
         self.assertEqual(args.INPUT_SEPARATOR, '\t')
diff --git a/tests/tpot_tests.py b/tests/tpot_tests.py
index a3dc156..72af036 100644
--- a/tests/tpot_tests.py
+++ b/tests/tpot_tests.py
@@ -168,7 +168,6 @@ def test_init_custom_parameters():
         cv=10,
         verbosity=1,
         random_state=42,
-        disable_update_check=True,
         warm_start=True,
         log_file=None
     )
diff --git a/tpot/base.py b/tpot/base.py
index 57e0a54..6c51d38 100644
--- a/tpot/base.py
+++ b/tpot/base.py
@@ -56,8 +56,6 @@ from sklearn.model_selection._split import check_cv
 
 from joblib import Parallel, delayed, Memory
 
-from update_checker import update_check
-
 from ._version import __version__
 from .operator_utils import TPOTOperatorClassFactory, Operator, ARGType
 from .export_utils import (
@@ -125,7 +123,6 @@ class TPOTBase(BaseEstimator):
         periodic_checkpoint_folder=None,
         early_stop=None,
         verbosity=0,
-        disable_update_check=False,
         log_file=None,
     ):
         """Set up the genetic programming algorithm for pipeline optimization.
@@ -272,8 +269,6 @@ class TPOTBase(BaseEstimator):
             How much information TPOT communicates while it's running.
             0 = none, 1 = minimal, 2 = high, 3 = all.
             A setting of 2 or higher will add a progress bar during the optimization procedure.
-        disable_update_check: bool, optional (default: False)
-            Flag indicating whether the TPOT version checker should be disabled.
         log_file: string, io.TextIOWrapper or io.StringIO, optional (defaul: sys.stdout)
             Save progress content to a file.
 
@@ -306,7 +301,6 @@ class TPOTBase(BaseEstimator):
         self.memory = memory
         self.use_dask = use_dask
         self.verbosity = verbosity
-        self.disable_update_check = disable_update_check
         self.random_state = random_state
         self.log_file = log_file
 
@@ -646,10 +640,6 @@ class TPOTBase(BaseEstimator):
         if self.max_time_mins is not None and self.generations is None:
             self.generations = 1000000
 
-        # Prompt the user if their version is out of date
-        if not self.disable_update_check:
-            update_check("tpot", __version__)
-
         if self.mutation_rate + self.crossover_rate > 1:
             raise ValueError(
                 "The sum of the crossover and mutation probabilities must be <= 1.0."
diff --git a/tpot/driver.py b/tpot/driver.py
index 600c710..dd2643a 100755
--- a/tpot/driver.py
+++ b/tpot/driver.py
@@ -473,14 +473,6 @@ def _get_arg_parser():
     )
 
 
-    parser.add_argument(
-        '--no-update-check',
-        action='store_true',
-        dest='DISABLE_UPDATE_CHECK',
-        default=False,
-        help='Flag indicating whether the TPOT version checker should be disabled.'
-    )
-
     parser.add_argument(
         '--version',
         action='version',
@@ -494,9 +486,7 @@ def _get_arg_parser():
 def _print_args(args):
     print('\nTPOT settings:')
     for arg, arg_val in sorted(args.__dict__.items()):
-        if arg == 'DISABLE_UPDATE_CHECK':
-            continue
-        elif arg == 'SCORING_FN' and arg_val is None:
+        if arg == 'SCORING_FN' and arg_val is None:
             if args.TPOT_MODE == 'classification':
                 arg_val = 'accuracy'
             else:
@@ -588,7 +578,6 @@ def tpot_driver(args):
         periodic_checkpoint_folder=args.CHECKPOINT_FOLDER,
         early_stop=args.EARLY_STOP,
         verbosity=args.VERBOSITY,
-        disable_update_check=args.DISABLE_UPDATE_CHECK,
         log_file=args.LOG
     )
 
