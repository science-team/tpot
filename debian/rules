#!/usr/bin/make -f
# -*- makefile -*-

# For DEB_VERSION_UPSTREAM
include /usr/share/dpkg/pkg-info.mk


export PYBUILD_NAME=tpot
export PYBUILD_TEST_NOSE=1

# Enable these once pytorch and xgboost are available
export PYBUILD_TEST_ARGS=--ignore-files nn_tests.py --exclude test_log_file_verbose_3
# This requires more analysis (it's been reported to upstream before)
PYBUILD_TEST_ARGS+=--exclude test_sample_weight_func
define PYBUILD_BEFORE_TEST
  cp -r {dir}/tests {build_dir};
  sed -i -e 's/python -m/{interpreter} -m/' {build_dir}/tests/driver_tests.py
endef
export PYBUILD_BEFORE_TEST

# This will be strippef from URLs in order to use the local versions
url_strip=https://raw.githubusercontent.com/EpistasisLab/tpot/master/


%:
	dh $@ --with python3,mkdocs --buildsystem=pybuild


override_dh_auto_build:
	dh_auto_build
	mkdocs build --clean --theme readthedocs
	rm -f docs/sitemap.xml.gz
	cp -r images docs/
	sed -i -e 's,$(url_strip),,' docs/index.html

override_dh_auto_install:
	dh_auto_install
	rm -f debian/python3-tpot/usr/lib/python3*/dist-packages/*.log
	rm -f debian/python3-tpot/usr/lib/python3*/dist-packages/progress.txt

override_dh_installdocs:
	dh_installdocs --package=python-tpot-doc --doc-main-package=python3-tpot
	dh_installdocs --remaining-packages

override_dh_installman:
	PYTHONPATH=. \
		help2man \
			--version-string="$(DEB_VERSION_UPSTREAM)" \
			-n 'Automated Machine Learning tool' \
			-N \
			-L C.UTF-8 \
			-o debian/tpot.1 \
			"debian/python3-tpot/usr/bin/tpot"
	dh_installman debian/tpot.1
