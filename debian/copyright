Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: TPOT
Source: http://epistasislab.github.io/tpot/installing/
Files-Excluded:
    tutorials/*
    docs/*
    ci/*
    images/tpot-logo.jpg
Comment:
 The tutorials and their data files have been removed. While the data files
 seem to be generally redistributable, their specific licensing terms were
 unclear, therefore it could not be determined whether they satisfy the terms
 of the DFSG or not.
 .
 The pre-built documentation has been removed, but only to avoid having to
 document the licensing terms of individual components generated/included by
 MkDocs.
 .
 The Travis CI integration scripts have been removed, as they claim to be a
 derivative, but the original source could not be tracked down.
 .
 The original Adobe Illustrator source for the logo is not available in the
 repository, it has been removed for now.

Files: *
Copyright: 2016-2020, Computational Genetics Lab at UPenn
                      Randal S. Olson <rso@randalolson.com>
                      Weixuan Fu <weixuanf@upenn.edu>
                      Daniel Angell <dpa34@drexel.edu>
                      and many more generous open source contributors
License: LGPL-3+

Files: tpot/builtins/one_hot_encoder.py
Copyright: 2014-2015, The auto-sklearn developers
License: BSD-3-clause

Files: tests/one_hot_encoder_tests.py
Copyright: 2014, Matthias Feurer
License: BSD-3-clause

Files: debian/*
Copyright: 2020, Christian Kastner <ckk@debian.org>
License: LGPL-3+

License: LGPL-3+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-3".

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 a. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 b. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 c. Neither the name of the auto-sklearn Developers nor the names of
    its contributors may be used to endorse or promote products
    derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
